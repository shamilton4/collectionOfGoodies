TRACKING_BRANCH=local/cox
REV=`git svn find-rev $(git rev-list --date-order --max-count=1 $TRACKING_BRANCH)`
git diff --no-prefix $(git rev-list --date-order --max-count=1 $TRACKING_BRANCH) $* |
sed -e "s/^+++ .*/&     (working copy)/" -e "s/^--- .*/&        (revision $REV)/" \
-e "s/^diff --git [^[:space:]]*/Index:/" \
-e "s/^index.*/===================================================================/" \
-e "s/\(\/dev\/null.*\)revision.*/\1revision 0)/" \
-e "s/^new file mode.*$//" |
sed -e "/\/dev\/null.*$/{
N
s/--- \/dev\/null[[:space:]]*\((revision 0)\).*\n.*+++ \(.*\).*\((working.*\)/--- \2 \1\\`echo -e '\n\r'`+++ \2 \3/
}"  |
tr -d '\r' |
sed -e "s/)+++/)\n+++/"


